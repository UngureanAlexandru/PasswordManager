#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <string.h>

struct userLoginData
{
	char name[100];
	char pass[100];
};

int main()
{
	struct sockaddr_in clientAddress;
	int clientSocket;
	struct userLoginData userData;
	int result = 0;
	char command[100];

	int port = 3500;
	char* ip = "127.0.0.1";

	clientSocket = socket(AF_INET, SOCK_STREAM, 0);

	clientAddress.sin_family = AF_INET;
	clientAddress.sin_addr.s_addr = inet_addr(ip);
	clientAddress.sin_port = htons(port);

	if (connect(clientSocket, (struct sockaddr *) &clientAddress, sizeof(struct sockaddr)) < 0)
	{
		printf("Error at connect !\n");
		return ;
	}

	printf("Name:");
	scanf("%s", userData.name);

	printf("Password:");
	scanf("%s", userData.pass);

	write(clientSocket, &userData, sizeof(struct userLoginData));

	result = -1;
	read(clientSocket, &result, sizeof(int));

	if (!result)
	{
		printf("User not found !\n");
		return ;
	}

	while(1)
	{
		printf("Enter command:");
		scanf("%s", command);

		write(clientSocket, command, 100);

		if (strcmp(command, "showCategories") == 0)
		{
			int sizeOfResult;
			char *categories;

			read (clientSocket, &sizeOfResult, sizeof(int));

			categories = malloc(sizeOfResult * sizeof(char));
			read (clientSocket, categories, sizeOfResult * sizeof(char));

			printf("Found categories: %s\n", categories);
			free (categories);
		}

		if (strcmp(command, "getPasswordByUsername") == 0)
		{
			char username[100];
			int sizeOfResult;
			char *passwords;

			printf("Username:");
			scanf("%s", username);

			write(clientSocket, username, 100);

			read (clientSocket, &sizeOfResult, sizeof(int));

			passwords = malloc(sizeOfResult * sizeof(char));
			read (clientSocket, passwords, sizeOfResult * sizeof(char));

			printf("Found passwords: %s\n", passwords);
			free (passwords);
		}

		if (strcmp(command, "getPasswordByTitle") == 0)
		{
			char title[100];
			int sizeOfResult;
			char *passwords;

			printf("Title:");
			scanf("%s", title);

			write(clientSocket, title, 100);

			read (clientSocket, &sizeOfResult, sizeof(int));

			passwords = malloc(sizeOfResult * sizeof(char));
			read (clientSocket, passwords, sizeOfResult * sizeof(char));

			printf("Found passwords: %s\n", passwords);
			free (passwords);
		}

		if (strcmp(command, "getPasswordByCategory") == 0)
		{
			char category[100];
			int sizeOfResult;
			char *passwords;

			printf("Category:");
			scanf("%s", category);

			write(clientSocket, category, 100);

			read (clientSocket, &sizeOfResult, sizeof(int));

			passwords = malloc(sizeOfResult * sizeof(char));
			read (clientSocket, passwords, sizeOfResult * sizeof(char));

			printf("Found passwords: %s\n", passwords);
			free (passwords);
		}

		if (strcmp(command, "getDataByUsername") == 0)
		{
			char username[100];
			char password[100];
			char title[100];
			char category[100];
			char notes[300];

			printf("Username:");
			scanf("%s", username);

			write(clientSocket, username, 100);
			int active = 1;

			while(active)
			{
				read(clientSocket, password, 100);
				if (strcmp(password, ".quit") == 0)
				{
					active = 0;
					break;
				}
				else
				{
					read(clientSocket, title, 100);
					read(clientSocket, category, 100);
					read(clientSocket, notes, 300);

					printf("Password: %s\n", password);
					printf("Title: %s\n", title);
					printf("Category: %s\n", category);
					printf("Notes: %s\n\n", notes);
				}
			}
		}

		if (strcmp(command, "getDataByTitle") == 0)
		{
			char username[100];
			char password[100];
			char title[100];
			char category[100];
			char notes[300];

			printf("Title:");
			scanf("%s", title);

			write(clientSocket, title, 100);
			int active = 1;

			while(active)
			{
				read(clientSocket, password, 100);
				if (strcmp(password, ".quit") == 0)
				{
					active = 0;
					break;
				}
				else
				{
					read(clientSocket, username, 100);
					read(clientSocket, category, 100);
					read(clientSocket, notes, 300);

					printf("Password: %s\n", password);
					printf("Username: %s\n", username);
					printf("Category: %s\n", category);
					printf("Notes: %s\n\n", notes);
				}
			}
		}

		if (strcmp(command, "getDataByCategory") == 0)
		{
			char username[100];
			char password[100];
			char title[100];
			char category[100];
			char notes[300];

			printf("Category:");
			scanf("%s", category);

			write(clientSocket, category, 100);
			int active = 1;

			while(active)
			{
				read(clientSocket, password, 100);
				if (strcmp(password, ".quit") == 0)
				{
					active = 0;
					break;
				}
				else
				{
					read(clientSocket, username, 100);
					read(clientSocket, title, 100);
					read(clientSocket, notes, 300);

					printf("Password: %s\n", password);
					printf("Username: %s\n", username);
					printf("Title: %s\n", title);
					printf("Notes: %s\n\n", notes);
				}
			}
		}



		if (strcmp(command, "insertPassword") == 0)
		{
			char password[100];
			char username[100];
			char title[100];
			char category[100];
			char notes[300];
			int success = -1;

			printf("Set password:");
			scanf("%s", password);

			printf("Set username:");
			scanf("%s", username);

			printf("Set title:");
			scanf("%s", title);

			printf("Set category:");
			scanf("%s", category);

			printf("Set note:");
			scanf("%s", notes);

			write(clientSocket, password, 100);
			write(clientSocket, username, 100);
			write(clientSocket, title, 100);
			write(clientSocket, category, 100);
			write(clientSocket, notes, 300);

			read(clientSocket, &success, sizeof(int));

			if (success)
			{
				printf("Data added to database !\n");
			}
			else
			{
				printf("Error !\n");
			}
		}

	}

	return 0;
}