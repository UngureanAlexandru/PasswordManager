#include <my_global.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <string.h>
#include <mysql.h>

void *userThread(void *params);

struct userLoginData
{
	char name[100];
	char pass[100];
};

struct data
{
	pthread_t userThread;
	int userSocket;
};
struct data userConnectionData[100];

int userIndex = 0;

MYSQL *con;

int main()
{
	int port = 3500;

	struct sockaddr_in serverAddress;
	struct sockaddr_in clientAddress;

	int serverSocket = socket(AF_INET, SOCK_STREAM, 0);
	int option = 1;
	setsockopt (serverSocket, SOL_SOCKET, SO_REUSEADDR, (void *) &option, sizeof (option));

	serverAddress.sin_family = AF_INET;
	serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddress.sin_port = htons(port);

	con = mysql_init(NULL);

	if (mysql_real_connect(con, "localhost", "root", "Darkness0", "userData", 0, NULL, 0) == NULL) 
	{
		fprintf(stderr, "%s\n", mysql_error(con));
		mysql_close(con);
	}  

	if (bind (serverSocket, (struct sockaddr *) &serverAddress, sizeof(struct sockaddr)) == -1)
	{
		printf("Can't bind !\n");
	}

	if (listen(serverSocket, 5) == -1)
	{
		printf("Can't listen !\n");
	}


	int clientAddressSize = sizeof(struct sockaddr_in);

	while(1)
	{
		int clientSocket = accept(serverSocket, (struct sockaddr *) &clientAddress, &clientAddressSize);

		if (clientSocket < 0)
		{
			printf("Error at accept !\n");
		}

		pthread_t tempUserThread;
		userConnectionData[userIndex].userThread = tempUserThread;
		userConnectionData[userIndex].userSocket = clientSocket;

		pthread_create(&userConnectionData[userIndex].userThread, NULL, (void*) userThread, (void*) &userConnectionData[userIndex]);
		userIndex++;
	}

	return 0;
}

void *userThread(void *params)
{
	struct data *connectionData = (struct data*) params;
	int clientSocket = connectionData->userSocket;

	struct userLoginData userData;

	// Read login data from client
	if (read(clientSocket, &userData, sizeof(struct userLoginData)) < 0)
	{
		printf("Can't read login data from user !\n");
	}

	// Create a query to check if the user is in the database
	char query[400];
	strcpy(query, "select * from users where name = '");
	strcat(query, userData.name);
	strcat(query, "' and password = '");
	strcat(query, userData.pass);
	strcat(query, "';");

	//Run the query
	if(mysql_query(con, query))
	{
		printf("MySQL query error !\n");
		return ;
	}

	// Get results
	MYSQL_RES *result = mysql_store_result(con);
	int num_fields = mysql_num_fields(result);

	MYSQL_ROW row;
	int userFound = 0;

	// Fetch results (translate in something we can use) and check if the user is in the database
	while ((row = mysql_fetch_row(result))) 
	{	 
		if (strcmp (row[1], userData.name) == 0 && strcmp(row[2], userData.pass) == 0)
		{
			userFound = 1;
		}
	}

	write(clientSocket, &userFound, sizeof(int));
	// If the user is not found, return ; (yes, this is legal in C)
	if (!userFound)
	{
		return ;
	}


	////// End login code ///////////////


	// Start commands code

	while (1)
	{
		// Initialize and read a command
		char command[100];
		if (read (clientSocket, command, 100) <= 0)
		{
			printf("Can't read command !\n");
			break;
		}

		printf("Command: %s\n", command);

		// We want to know what categories we have
		if (strcmp(command, "showCategories") == 0)
		{
			// Create a query to select the categories
			char query[400];
			strcpy(query, "select distinct category from testTable where");
			strcat(query, " name = '");
			strcat(query, userData.name);
			strcat(query, "' and pass = '");
			strcat(query, userData.pass);
			strcat(query, "';");

			if(mysql_query(con, query))
			{
				printf("MySQL query error !\n");
				return ;
			}

			// Get results
			MYSQL_RES *result = mysql_store_result(con);
			int num_fields = mysql_num_fields(result);

			MYSQL_ROW row;
			char* foundCategories;
			foundCategories = calloc(sizeof(char) , 1);

			// Fetch results (translate in something we can use).
			while ((row = mysql_fetch_row(result))) 
			{	 
			// Resize and concatenate the list
				foundCategories = realloc (foundCategories, sizeof(char) * (strlen(foundCategories) + strlen(row[0]) ) + 1);
				strcat(foundCategories, row[0]);
				strcat(foundCategories, ", ");
			}
			// Send the size of the list to anounce the client how many bytes it must read.
			int sizeOfResult = strlen(foundCategories) * sizeof(char);
			write(clientSocket, &sizeOfResult, sizeof(int));

			// Send the list of categories
			write(clientSocket, foundCategories, sizeOfResult);

			//free(foundCategories);
		}

		if (strcmp(command, "getPasswordByUsername") == 0)
		{
			char username[100];

			read(clientSocket, username, 100);

			// Create the query to get the passwirds that have the specified username.
			char query[400];
			strcpy(query, "select password from testTable where username = '");
			strcat(query, username);
			strcat(query, "' and name = '");
			strcat(query, userData.name);
			strcat(query, "' and pass = '");
			strcat(query, userData.pass);
			strcat(query, "';");
			if(mysql_query(con, query))
			{
				printf("MySQL query error !\n");
				return ;
			}

			// Get results
			MYSQL_RES *result = mysql_store_result(con);
			int num_fields = mysql_num_fields(result);

			MYSQL_ROW row;
			char* foundPasswords = calloc(sizeof(char), 1);

			// Fetch results (translate in something we can use).
			while ((row = mysql_fetch_row(result))) 
			{	 
			// Resize and concatenate the list
				foundPasswords = realloc (foundPasswords, sizeof(char) * (strlen(foundPasswords) + strlen(row[0]) ) + 1);
				strcat(foundPasswords, row[0]);
				strcat(foundPasswords, ", ");

			}

			// Send the size of the list to anounce the client how many bytes it must read.
			int sizeOfResult = strlen(foundPasswords);
			write(clientSocket, &sizeOfResult, sizeof(int));

			// Send the list of passwords
			write(clientSocket, foundPasswords, sizeOfResult);

			free(foundPasswords);
		}

		if (strcmp(command, "getPasswordByTitle") == 0)
		{
			char title[100];

			read(clientSocket, title, 100);

			// Create the query to get the passwirds that have the specified title.
			char query[400];
			strcpy(query, "select password from testTable where title = '");
			strcat(query, title);
			strcat(query, "' and name = '");
			strcat(query, userData.name);
			strcat(query, "' and pass = '");
			strcat(query, userData.pass);
			strcat(query, "';");

			if(mysql_query(con, query))
			{
				printf("MySQL query error !\n");
				return ;
			}

			// Get results
			MYSQL_RES *result = mysql_store_result(con);
			int num_fields = mysql_num_fields(result);

			MYSQL_ROW row;
			char* foundPasswords = malloc(sizeof(char) * 1);

			// Fetch results (translate in something we can use).
			while ((row = mysql_fetch_row(result))) 
			{	 
			// Resize and concatenate the list
				foundPasswords = realloc (foundPasswords, sizeof(char) * (strlen(foundPasswords) + strlen(row[0]) ) + 1);
				strcat(foundPasswords, row[0]);
				strcat(foundPasswords, ", ");
			}

			// Send the size of the list to anounce the client how many bytes it must read.
			int sizeOfResult = strlen(foundPasswords);
			write(clientSocket, &sizeOfResult, sizeof(int));

			// Send the list of passwords
			write(clientSocket, foundPasswords, sizeOfResult);

			free(foundPasswords);
		}

		if (strcmp(command, "getPasswordByCategory") == 0)
		{
			char category[100];

			read(clientSocket, category, 100);

			// Create the query to get the passwirds that have the specified category.
			char query[400];
			strcpy(query, "select password from testTable where category = '");
			strcat(query, category);
			strcat(query, "' and name = '");
			strcat(query, userData.name);
			strcat(query, "' and pass = '");
			strcat(query, userData.pass);
			strcat(query, "';");

			if(mysql_query(con, query))
			{
				printf("MySQL query error !\n");
				return ;
			}

			// Get results
			MYSQL_RES *result = mysql_store_result(con);
			int num_fields = mysql_num_fields(result);

			MYSQL_ROW row;
			char* foundPasswords = malloc(sizeof(char) * 1);

			// Fetch results (translate in something we can use).
			while ((row = mysql_fetch_row(result))) 
			{	 
			// Resize and concatenate the list
				foundPasswords = realloc (foundPasswords, sizeof(char) * (strlen(foundPasswords) + strlen(row[0]) ) + 1);
				strcat(foundPasswords, row[0]);
				strcat(foundPasswords, ", ");
			}

			// Send the size of the list to anounce the client how many bytes it must read.
			int sizeOfResult = strlen(foundPasswords);
			write(clientSocket, &sizeOfResult, sizeof(int));

			// Send the list of passwords
			write(clientSocket, foundPasswords, sizeOfResult);

			free(foundPasswords);
		}


////////////


		if (strcmp(command, "getDataByUsername") == 0)
		{
			char username[100];

			read(clientSocket, username, 100);

			// Create the query to get the passwirds that have the specified username.
			char query[400];
			strcpy(query, "select * from testTable where username = '");
			strcat(query, username);
			strcat(query, "' and name = '");
			strcat(query, userData.name);
			strcat(query, "' and pass = '");
			strcat(query, userData.pass);
			strcat(query, "';");

			if(mysql_query(con, query))
			{
				printf("MySQL query error !\n");
				return ;
			}

			// Get results
			MYSQL_RES *result = mysql_store_result(con);
			int num_fields = mysql_num_fields(result);

			MYSQL_ROW row;

			// Fetch results (translate in something we can use).
			while ((row = mysql_fetch_row(result))) 
			{	 
			// Resize and concatenate the list
				write(clientSocket, row[3], 100);
				write(clientSocket, row[5], 100);
				write(clientSocket, row[6], 100);
				write(clientSocket, row[7], 300);
			}

			char quit[100];
			strcpy(quit, ".quit");
			write(clientSocket, quit, 100);
		}

		if (strcmp(command, "getDataByTitle") == 0)
		{
			char title[100];

			read(clientSocket, title, 100);

			// Create the query to get the passwirds that have the specified title.
			char query[400];
			strcpy(query, "select * from testTable where title = '");
			strcat(query, title);
			strcat(query, "' and name = '");
			strcat(query, userData.name);
			strcat(query, "' and pass = '");
			strcat(query, userData.pass);
			strcat(query, "';");

			if(mysql_query(con, query))
			{
				printf("MySQL query error !\n");
				return ;
			}

			// Get results
			MYSQL_RES *result = mysql_store_result(con);
			int num_fields = mysql_num_fields(result);

			MYSQL_ROW row;

			// Fetch results (translate in something we can use).
			while ((row = mysql_fetch_row(result))) 
			{	 
			// Resize and concatenate the list
				write(clientSocket, row[3], 100);
				write(clientSocket, row[4], 100);
				write(clientSocket, row[6], 100);
				write(clientSocket, row[7], 300);
			}

			char quit[100];
			strcpy(quit, ".quit");
			write(clientSocket, quit, 100);
		}

		if (strcmp(command, "getDataByCategory") == 0)
		{
			char category[100];

			read(clientSocket, category, 100);

			// Create the query to get the passwirds that have the specified category.
			char query[400];
			strcpy(query, "select * from testTable where category = '");
			strcat(query, category);
			strcat(query, "' and name = '");
			strcat(query, userData.name);
			strcat(query, "' and pass = '");
			strcat(query, userData.pass);
			strcat(query, "';");

			if(mysql_query(con, query))
			{
				printf("MySQL query error !\n");
				return ;
			}

			// Get results
			MYSQL_RES *result = mysql_store_result(con);
			int num_fields = mysql_num_fields(result);

			MYSQL_ROW row;

			while ((row = mysql_fetch_row(result))) 
			{	 
			// Resize and concatenate the list
				write(clientSocket, row[3], 100);
				write(clientSocket, row[4], 100);
				write(clientSocket, row[5], 100);
				write(clientSocket, row[7], 300);
			}

			char quit[100];
			strcpy(quit, ".quit");
			write(clientSocket, quit, 100);
		}


		//////////
		if (strcmp(command, "insertPassword") == 0)
		{
			int success = 1;

			char password[100];
			char username[100];
			char title[100];
			char category[100];
			char notes[300];

			read(clientSocket, password, 100);

			read(clientSocket, username, 100);

			read(clientSocket, title, 100);

			read(clientSocket, category, 100);

			read(clientSocket, notes, 300);

			// Create a query to check if the user is in the database
			char query[400];
			strcpy(query, "insert into testTable (name, pass, password, username, title, category, notes) values ('");
			strcat(query, userData.name);
			strcat(query, "', '");
			strcat(query, userData.pass);
			strcat(query, "', '");
			strcat(query, password);
			strcat(query, "', '");
			strcat(query, username);
			strcat(query, "','");
			strcat(query, title);
			strcat(query, "','");
			strcat(query, category);
			strcat(query, "','");
			strcat(query, notes);
			strcat(query, "');");

			printf("Query %s\n", query);

			//Run the query
			if(mysql_query(con, query))
			{
				success = 0;
				write(clientSocket, &success, sizeof(int));
				printf("MySQL query error !\n");
				return ;
			}

			write(clientSocket, &success, sizeof(int));

		}
	}
	
}
